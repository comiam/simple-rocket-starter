# simple-rocket-starter

Классический пример стартера в Spring Boot. Использует новый подход [авто конфигурации](https://docs.spring.io/spring-boot/docs/current/reference/html/features.html#features.developing-auto-configuration.locating-auto-configuration-candidates).

Для старого метода со `spring.factories` надо:
- создать в ресурсах `META-INF/spring.factories`
- прописать туда:
```
org.springframework.boot.autoconfigure.EnableAutoConfiguration=\
  ru.rcktsci.rocketstarter.config.RocketConfiguration
```

Генерация метаинфы пропертей так же тут есть:)

[Презентация к проекту](https://docs.google.com/presentation/d/1WJRQNxy255z2_ORKuVuLaEfE3onPk_b3hWaeAskvTPw/edit?usp=sharing).

