package ru.rcktsci.spacestation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.rcktsci.rocketstarter.annotation.AllowToInviteRocketWatchers;
import ru.rcktsci.rocketstarter.annotation.RocketWatcher;
import ru.rcktsci.rocketstarter.component.RocketCorpus;
import ru.rcktsci.rocketstarter.component.RocketEngine;
import ru.rcktsci.rocketstarter.component.RocketStorage;

import java.util.Map;

@SpringBootApplication
@AllowToInviteRocketWatchers
public class SpaceStationApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpaceStationApplication.class, args);
    }
}

@Configuration
class SpaceStationConfig {
    @Bean
    RocketCorpus getCorpus() {
        return new RocketCorpus("мячик", "футбольный");
    }

    @Bean
    RocketEngine getEngine() {
        return new RocketEngine("конь", "лошадиная", "ванная с огурцами");
    }

    @Bean
    RocketStorage getStorage() {
        return new RocketStorage(Map.of("чокопайка с апельсином", 50L, "еда на пятник", 150L));
    }
}

@RocketWatcher(name = "Андрей")
class Watcher1 {
}

@RocketWatcher(name = "Рома")
class Watcher2 {
}
