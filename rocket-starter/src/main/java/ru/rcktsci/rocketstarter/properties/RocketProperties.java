package ru.rcktsci.rocketstarter.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

@Validated
@ConfigurationProperties(prefix = "rocket")
public record RocketProperties(
        @Min(value = 0, message = "liftingWeight: Подъёмная масса должна быть более 0 кг!")
        long liftingWeight,
        @Min(value = 1, message = "flightTime: Ракета должна взлететь хотя бы на минуту блин:\\")
        long flightTime,
        @NotBlank(message = "rocketName: Ну ты уж назови внятно ракету то..")
        String rocketName
) {
}
