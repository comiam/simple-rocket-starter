package ru.rcktsci.rocketstarter.config;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.rcktsci.rocketstarter.component.Rocket;
import ru.rcktsci.rocketstarter.component.RocketCorpus;
import ru.rcktsci.rocketstarter.component.RocketEngine;
import ru.rcktsci.rocketstarter.component.RocketStorage;
import ru.rcktsci.rocketstarter.properties.RocketProperties;

import java.util.Collections;

@Slf4j
@Configuration
@RequiredArgsConstructor
@EnableConfigurationProperties(RocketProperties.class)
@ConditionalOnProperty(prefix = "rocket", name = {"rocket-name", "flight-time", "lifting-weight"})
public class RocketConfiguration {
    private final RocketProperties rocketProperties;

    @Bean
    @ConditionalOnMissingBean({RocketStorage.class, RocketEngine.class, RocketCorpus.class})
    Rocket buildDefaultRocket() {
        log.info("Собираем ракету!");
        log.info("Ну... кастомных компонент нет, так что соберём обычную ракету.");

        var rocket = new Rocket(rocketProperties,
                new RocketCorpus("цилиндр", "белый"),
                new RocketEngine("мощная", "самый раз", "из бака машины соседа"),
                new RocketStorage(Collections.emptyMap()));

        log.info("\nДАННЫЕ РАКЕТЫ:\n{}", rocket);

        return rocket;
    }

    @Bean
    @ConditionalOnBean({RocketStorage.class, RocketEngine.class, RocketCorpus.class})
    Rocket buildCustomRocket(@Autowired RocketStorage rocketStorage, @Autowired RocketEngine rocketEngine,
                             @Autowired RocketCorpus rocketCorpus) {
        log.info("Собираем ракету!");
        log.info("Ого, собираем что-то своё! Ну давай:)");

        if (rocketStorage.isOverloadOnRocket(rocketProperties.liftingWeight())) {
            throw new RuntimeException("Ракета \"%s\" не потянет такой вес, какой ты задал в хранилище: %d".formatted(
                    rocketProperties.rocketName(), rocketStorage.getTotalStorageWeight()
            ));
        }

        var rocket = new Rocket(rocketProperties, rocketCorpus, rocketEngine, rocketStorage);

        log.info("\nДАННЫЕ РАКЕТЫ:\n{}", rocket);

        return rocket;
    }

    @Bean
    RocketStarterListener getRocketListener(@Autowired Rocket rocket, @Autowired ApplicationContext context) {
        return new RocketStarterListener(rocket, context);
    }
}
