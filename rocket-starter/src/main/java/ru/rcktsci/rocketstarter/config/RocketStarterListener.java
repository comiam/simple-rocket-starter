package ru.rcktsci.rocketstarter.config;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import ru.rcktsci.rocketstarter.annotation.RocketWatcher;
import ru.rcktsci.rocketstarter.component.Rocket;


@AllArgsConstructor
@Slf4j
public class RocketStarterListener implements ApplicationListener<ContextRefreshedEvent> {
    private Rocket rocket;
    private ApplicationContext context;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        var names = context.getBeanNamesForAnnotation(RocketWatcher.class);

        for (var name : names) {
            var bean = context.getBean(name);
            log.info("На ракету придёт смотреть {}!", bean.getClass().getAnnotation(RocketWatcher.class).name());
        }

        log.info("Ракета \"{}\" успешно стартанула! {} зрителей в шоке!", rocket.rocketProperties().rocketName(), names.length);
    }
}

