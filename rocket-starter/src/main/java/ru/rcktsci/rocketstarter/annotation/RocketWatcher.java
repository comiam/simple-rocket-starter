package ru.rcktsci.rocketstarter.annotation;

import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface RocketWatcher {
    String name() default "anonymous-watcher";
}
