package ru.rcktsci.rocketstarter.annotation;

import org.springframework.context.annotation.Import;
import ru.rcktsci.rocketstarter.config.RocketWatchersBeanDefinitionRegistrar;

import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Import(RocketWatchersBeanDefinitionRegistrar.class)
public @interface AllowToInviteRocketWatchers {
}
