package ru.rcktsci.rocketstarter.component;

import ru.rcktsci.rocketstarter.properties.RocketProperties;

public record Rocket(RocketProperties rocketProperties, RocketCorpus rocketCorpus,
                     RocketEngine rocketEngine, RocketStorage rocketStorage) {
    @Override
    public String toString() {
        return """
                Имя: %s
                Подъёмная сила: %s кг
                Время полёта: %s минут
                %s%s%s
                """.formatted(
                rocketProperties.rocketName(),
                rocketProperties.liftingWeight(),
                rocketProperties.flightTime(),
                rocketCorpus.toString(),
                rocketEngine.toString(),
                rocketStorage.toString()
        );
    }
}
