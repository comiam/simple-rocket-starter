package ru.rcktsci.rocketstarter.component;

import lombok.AllArgsConstructor;
import lombok.Value;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
@Value
public class RocketEngine {
    String model;
    String power;
    String fuelType;

    @Override
    public String toString() {
        return """
                Параметры двигателя:
                    - модель: %s
                    - мощность: %s
                    - тип топлива: %s
                """.formatted(model, power, fuelType);
    }
}
