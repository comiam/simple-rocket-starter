package ru.rcktsci.rocketstarter.component;

import lombok.AllArgsConstructor;
import lombok.Value;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
@AllArgsConstructor
@Value
public class RocketStorage {
    Map<String, Long> storageBags;

    public boolean isOverloadOnRocket(Long currentLiftingWeight) {
        var totalStorageWeight = getTotalStorageWeight();

        return totalStorageWeight > currentLiftingWeight;
    }

    public Long getTotalStorageWeight() {
        var totalStorageWeight = storageBags.values().stream().reduce(Long::sum);

        return totalStorageWeight.orElse(0L);
    }

    @Override
    public String toString() {
        StringBuilder data = new StringBuilder("Груз ракеты:");

        if (storageBags.isEmpty()) {
            data.append("\nПустовато...");
        } else {
            for (var bag : storageBags.entrySet()) {
                data.append("\n");
                data.append("    - %s в количестве %d кг".formatted(bag.getKey(), bag.getValue()));
            }
        }

        return data.toString();
    }
}
