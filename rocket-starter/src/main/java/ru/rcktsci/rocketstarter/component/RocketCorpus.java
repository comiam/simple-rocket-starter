package ru.rcktsci.rocketstarter.component;

import lombok.AllArgsConstructor;
import lombok.Value;
import org.springframework.stereotype.Component;

@Component
@Value
@AllArgsConstructor
public class RocketCorpus {
    String form;
    String color;

    @Override
    public String toString() {
        return """
                Параметры корпуса:
                    - форма корпуса: %s
                    - цвет: %s
                """.formatted(form, color);
    }
}
